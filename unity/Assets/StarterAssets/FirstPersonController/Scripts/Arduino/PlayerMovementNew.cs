using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovementNew : MonoBehaviour
{
    [Header("Movement")]
    public float moveSpeed;

    public float groundDrag;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    bool grounded;

    public LayerMask whatIsPain;
    bool resistance;

    public Transform orientation;

    // float horizontalInput;
    // float verticalInput;

    ArduinoCommunicator arduinoCommunicator;
    int upInput;
    int rightInput;
    int downInput;
    int leftInput;

    int upSpeed;
    int rightSpeed;
    int downSpeed;
    int leftSpeed;

    Vector3 moveDirection;

    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    private void Update()
    {
        // ground check
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsGround);

        // check for resistance
        resistance = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsPain);

        MyInput();

        // handle drag
        if (grounded){
            rb.drag = groundDrag;
        } else {
            rb.drag = 0;
        }
    }

    private void FixedUpdate()
    {
        MovePlayer();
        SpeedControl();
    }

    void OnCollisionStay (Collision targetObj) {

        if (targetObj.gameObject.tag == "resistanceHard"){
            gameObject.GetComponent<ArduinoCommunicator>().SendResistance("40", "40", "40", "40");
            // TODO: setze Widerstand herab, wenn Spieler abwärts läuft
        } else if (targetObj.gameObject.tag == "resistanceLow") {
            gameObject.GetComponent<ArduinoCommunicator>().SendResistance("20", "20", "20", "20");
            // TODO: setze Widerstand herab, wenn Spieler abwärts läuft
        } else if (targetObj.gameObject.tag == "resistanceWater") {
            gameObject.GetComponent<ArduinoCommunicator>().SendResistance("100", "100", "100", "100");
        } else if (targetObj.gameObject.tag == "resistanceWind") {
            gameObject.GetComponent<ArduinoCommunicator>().SendResistance("100", "100", "100", "100");
            // TODO: setze Widerstand herab, wenn Spieler
            //          - sich aus dem Windkanal herausbewegt
            //          - sich orthogonal zum Wind bewegt
            //       bewege Spieler aus Windkanal heraus, wenn keine Bewegung
        } else if (targetObj.gameObject.tag == "whatIsGround") {
            gameObject.GetComponent<ArduinoCommunicator>().SendResistance("10", "10", "10", "10");
        }
    }

    private void MyInput()
    {
        // get input from digital keyboard
        // horizontalInput = Input.GetAxisRaw("Horizontal");
        // verticalInput = Input.GetAxisRaw("Vertical");

        // get input from analgo keyboard
        upInput = gameObject.GetComponent<ArduinoCommunicator>().upKeyPos;
        rightInput = gameObject.GetComponent<ArduinoCommunicator>().rightKeyPos;
        downInput = gameObject.GetComponent<ArduinoCommunicator>().downKeyPos;
        leftInput = gameObject.GetComponent<ArduinoCommunicator>().leftKeyPos;

        upSpeed = gameObject.GetComponent<ArduinoCommunicator>().upKeyVelocity;
        rightSpeed = gameObject.GetComponent<ArduinoCommunicator>().upKeyVelocity;
        downSpeed = gameObject.GetComponent<ArduinoCommunicator>().upKeyVelocity;
        leftSpeed = gameObject.GetComponent<ArduinoCommunicator>().upKeyVelocity;
    }

    private void MovePlayer()
    {
        // calculate movement direction from digital input
        // moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;

        // calculate movement direction and speed from analog input
        moveDirection = orientation.forward * (upInput - downInput) + orientation.right * (rightInput - leftInput);

        if (upInput > downInput && upInput > rightInput && upInput > leftInput) {
            moveSpeed = upInput;
        } 
        else if (rightInput > downInput && rightInput > upInput && rightInput > leftInput) {
            moveSpeed = rightInput;
        }
        else if (downInput > rightInput && downInput > upInput && downInput > leftInput) {
            moveSpeed = downInput;
        }
        else if (leftInput > downInput && leftInput > rightInput && leftInput > downInput) {
            moveSpeed = leftInput;
        }
        rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);
    }

    private void SpeedControl() {

        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        // limit velocity if needed
        if (flatVel.magnitude > moveSpeed) {

            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }
}